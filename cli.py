import click

from src import config, dataset, demo_camera, labelbox


@click.group()
def entry_point():
    pass


@entry_point.command()
@click.option(
    "--data-dir",
    default="data",
    type=click.Path(exists=False, file_okay=False, dir_okay=True),
)
@click.option("--host", type=str, default="0.0.0.0")  # nosec
@click.option("--port", type=int, default=8080)
def api(data_dir: str, host: str, port: int) -> None:
    config.set_root_data_dir(data_dir)
    # import here so that the camera module is only instantiated when necessary
    from src import api

    api.app.run(host, port)


entry_point.add_command(demo_camera.demo_camera)
entry_point.add_command(dataset.dataset)
entry_point.add_command(labelbox.labelbox)

if __name__ == "__main__":
    entry_point()
