PYTHON_FILES := *.py src/*.py tests/*.py env/*.py
PORT := 8080
DATA_DIR := data

.DEFAULT_GOAL := api

.PHONY: setup
setup:
	pip3 install --user pipenv
	pipenv install


.PHONY: setup-dev
setup-dev: setup
	pipenv install --dev
	# if you receive an error like
	# RuntimeError: failed to find interpreter for Builtin discover of python_spec='python3.6m'
	# try editing .venv/lib/python3.6/site-packages/pre_commit/languages:make_venv
	# insert
	# python = "$(pwd)/.venv/bin/python3.6m" (evaluate pwd manually)
	# before the line
	# cmd = (sys.executable, '-mvirtualenv', envdir, '-p', python)
	pipenv run pre-commit install --install-hooks


.PHONY: update-requirements
update-requirements:
	pipenv run pip freeze > requirements.txt


.PHONY: fmt
fmt:
	pipenv run isort --recursive $(PYTHON_FILES)
	pipenv run black $(PYTHON_FILES)


.PHONY: test
test: fmt
	pipenv run python3 -m pytest
	pipenv run pre-commit run --all-files


.PHONY: demo
demo:
	pipenv run python3 demo_camera.py


clean-raw: SUB_DIR := raw
clean-extracted: SUB_DIR := extracted
.PHONY: clean-raw clean-extracted
clean clean-extracted:
	rm $(DATA_DIR)/$(SUB_DIR) -rf


.PHONY: dev
dev:
	FLASK_APP=api.py FLASK_DEBUG=true FLASK_ENV=development pipenv run flask run --host=0.0.0.0 --port=$(PORT)


.PHONY: prod
prod:
	pipenv run python3 win10.py


extract: FLAG := --no-separate-channel-extraction
extract-separately: FLAG := --separate-channel-extraction
.PHONY: extract extract-separately
extract extract-separately:
	pipenv run python3 extract_from_raw.py $(DATA_DIR) $(FLAG)


.PHONY: jupyter
jupyter:
	pipenv run jupyter notebook
