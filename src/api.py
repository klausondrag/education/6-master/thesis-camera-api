import cv2
import nest_asyncio
import numpy as np
from PIL import Image
from quart import Quart, Response, jsonify, send_file
from quart.logging import default_handler

from src import camera, config


nest_asyncio.apply()

default_handler.setFormatter(config.LOG_FORMAT)
default_handler.setLevel(config.LOG_LEVEL)
app = Quart(__name__)

global_device_manager = None if config.IS_DEV_ENV else camera.DeviceManager()


@app.route("/health")
def health() -> Response:
    return jsonify(dict(status="ok"))


@app.route("/images")
def list_images_ids() -> Response:
    ids = [p.name for p in config.RAW_DATA_DIR.iterdir()]
    ids = list(set(ids))
    ids = sorted(ids)
    ids = ids[::-1]
    return jsonify(ids)


@app.route("/take-picture")
def take_picture() -> Response:
    global global_device_manager
    local_device_manager = (
        camera.DeviceManager() if config.IS_DEV_ENV else global_device_manager
    )
    image_id = local_device_manager.get_frames()
    return jsonify(dict(status="ok", n_tries=1, image_id=image_id))


@app.route("/images/<string:image_id>/color")
def get_color_image(image_id: str) -> Response:
    return _get_image(image_id, config.DEFAULT_PRESET_NAME, config.ChannelMode.COLOR)


@app.route("/images/<string:image_id>/depth")
def get_depth_image(image_id: str) -> Response:
    return _get_image(image_id, config.DEFAULT_PRESET_NAME, config.ChannelMode.DEPTH)


@app.route("/images/<string:image_id>/<string:preset_name>/color")
def get_color_preset_image(image_id: str, preset_name: str) -> Response:
    return _get_image(image_id, preset_name, config.ChannelMode.COLOR)


@app.route("/images/<string:image_id>/<string:preset_name>/depth")
def get_depth_preset_image(image_id: str, preset_name: str) -> Response:
    return _get_image(image_id, preset_name, config.ChannelMode.DEPTH)


def _get_image(
    image_id: str, preset_name: str, channel_mode: config.ChannelMode
) -> Response:
    """(
        f"{unique_name}"
        f"_{preset_name}"
        f"_{channel_mode.value}"
        f".{config.IMAGE_FILE_EXTENSION}"
    )
    """
    image_dir = config.RAW_DATA_DIR / image_id
    files = image_dir.glob(
        f"*"
        f"_{preset_name}"
        f"_{channel_mode.value}"
        f".{config.IMAGE_FILE_EXTENSION}"
    )
    unique_names = [f.name[: f.name.index("_")] for f in files]
    app.logger.info(f"Found unique names: {unique_names}")

    files_paths = [
        camera.get_file_path(
            config.RAW_DATA_DIR, image_id, uq, preset_name, channel_mode
        )
        for uq in unique_names
    ]
    images = [cv2.imread(str(fp)) for fp in files_paths]
    images = [cv2.cvtColor(image, cv2.COLOR_RGB2BGR) for image in images]

    thumbnail_path = config.THUMBNAILS_DATA_DIR / (
        f"{image_id}" f"_{channel_mode.value}" f".{config.IMAGE_FILE_EXTENSION}"
    )

    if not thumbnail_path.exists():
        merged_image_numpy = np.concatenate(images, axis=0)
        merged_image = Image.fromarray(merged_image_numpy)
        file_format = (
            "JPEG"
            if config.IMAGE_FILE_EXTENSION.lower() == "jpg"
            else config.IMAGE_FILE_EXTENSION
        )
        merged_image.save(thumbnail_path, file_format, quality=80)

    return send_file(str(thumbnail_path))
