import datetime
import json
import logging
import random
import uuid
from pathlib import Path
from typing import Dict, List, Optional

import click
import cv2
import graphqlclient

from env import secrets
from src import config


logging.basicConfig(format=config.LOG_FORMAT, level=config.LOG_LEVEL)
logger = logging.getLogger(__name__)


@click.group()
@click.pass_context
def labelbox(context: click.core.Context) -> None:
    context.ensure_object(dict)

    client = graphqlclient.GraphQLClient(config.LABELBOX_API_URL)
    client.inject_token(f"Bearer {secrets.API_KEY}")
    context.obj["client"] = client


@labelbox.command()
@click.pass_context
def test_connection(context: click.core.Context) -> None:
    client = context.obj["client"]
    pretty_print(
        _get_json(
            client,
            """
               query GetUserInformation {
                 user {
                   id
                   organization{
                     id
                   }
                 }
               }
            """,
        )
    )


@labelbox.command()
@click.pass_context
def list_projects(context: click.core.Context) -> None:
    client = context.obj["client"]
    pretty_print(
        _get_json(
            client,
            """
            query AllProjects{
                projects{
                    id
                    name
                }
            }
            """,
        )
    )


@labelbox.command()
@click.pass_context
def list_datasets(context: click.core.Context) -> None:
    client = context.obj["client"]
    pretty_print(
        _get_json(
            client,
            """
            query AllDatasets{
              datasets{
                id
                name
              }
            }
            """,
        )
    )


@labelbox.command()
@click.argument(
    "project-id", type=str,
)
@click.argument(
    "dataset-id", type=str,
)
@click.argument(
    "model-output-json",
    default="data/example_model_output.json",
    type=click.Path(exists=True, file_okay=True, dir_okay=False),
)
@click.option(
    "--sample-external-id", default=None, type=str,
)
@click.pass_context
def convert(
    context: click.core.Context,
    project_id: str,
    dataset_id: str,
    model_output_json: str,
    sample_external_id: Optional[str],
) -> None:
    client = context.obj["client"]

    model_output_json_file_path = Path(model_output_json)
    with open(model_output_json_file_path, "r") as file:
        model_data = json.load(file)

    if sample_external_id is not None:
        model_data["images"] = [
            image
            for image in model_data["images"]
            if image["file_name"] == sample_external_id
        ]
        previous_image_id = model_data["images"][0]["id"]
        new_image_id = 0
        model_data["images"][0]["id"] = new_image_id

        new_annotations = []
        annotation_counter = 0
        for annotation in model_data["annotations"]:
            if annotation["image_id"] == previous_image_id:
                annotation["image_id"] = new_image_id
                annotation["id"] = annotation_counter
                new_annotations.append(annotation)
                annotation_counter += 1

        model_data["annotations"] = new_annotations

    image_id_to_data_row_id = get_conversion_dict(client, dataset_id, model_data)

    output_rows = get_converted_rows(
        client, project_id, model_data, image_id_to_data_row_id
    )

    output_file_path = model_output_json_file_path.parent / (
        f"{model_output_json_file_path.stem}"
        f"_{config.LABELBOX_SUFFIX}"
        f".{config.LABELBOX_EXTENSION}"
    )
    with open(output_file_path, "w") as file:
        for output_row in output_rows:
            file.write(json.dumps(output_row))
            file.write("\n")


@labelbox.command()
@click.argument(
    "project-id", type=str,
)
@click.argument(
    "file-url", type=str,
)
@click.pass_context
def create_request(context: click.core.Context, project_id: str, file_url: str) -> None:
    client = context.obj["client"]

    import_request_name = create_bulk_import_request(client, project_id, file_url)
    logger.info(f"Request ID: {import_request_name}")

    while True:
        logger.info(f"Polling status... Exit the program manually when done.")
        status(context, project_id, import_request_name)


@labelbox.command()
@click.argument(
    "project-id", type=str,
)
@click.argument(
    "import-request-name", type=str,
)
@click.pass_context
def status(context: click.core.Context, project_id: str, import_request_name: str):
    client = context.obj["client"]

    data = get_import_request_update(client, project_id, import_request_name)
    print(data)
    print(data["data"]["bulkImportRequest"]["state"])


@labelbox.command()
@click.argument(
    "data-dir",
    default="data",
    type=click.Path(exists=True, file_okay=False, dir_okay=True),
)
@click.argument(
    "annotations-json", type=click.Path(exists=True, file_okay=True, dir_okay=False),
)
@click.argument(
    "all-file", type=click.Path(exists=True, file_okay=True, dir_okay=False),
)
def labels_to_coco(
    data_dir: str, annotations_json: str, all_file: str, val_percentage: float = 0.2
) -> None:
    config.set_root_data_dir(data_dir)

    input_file_path = Path(annotations_json)
    train_output_file_path = (
        input_file_path.parent / f"{input_file_path.stem}_coco_train.json"
    )
    val_output_file_path = (
        input_file_path.parent / f"{input_file_path.stem}_coco_val.json"
    )
    test_output_file_path = (
        input_file_path.parent / f"{input_file_path.stem}_coco_test.json"
    )

    with open(str(input_file_path), "r") as file:
        input_data = json.load(file)

    with open(str(all_file), "r") as file:
        all_data = json.load(file)

    categories = dict()
    category_counter = 0

    used_file_names = set()
    train_images = []
    val_images = []
    train_annotations = []
    val_annotations = []
    train_image_counter = 0
    val_image_counter = 0
    train_annotation_counter = 0
    val_annotation_counter = 0
    for labelbox_image in input_data:
        if "objects" not in labelbox_image["Label"]:
            continue

        is_validation = random.random() < val_percentage
        image_counter_value = (
            val_image_counter if is_validation else train_image_counter
        )

        file_name = labelbox_image["External ID"]
        used_file_names.add(file_name)
        id_path = config.FLAT_IMAGES_DATA_DIR / file_name
        image = cv2.imread(str(id_path))
        height, width, _ = image.shape
        image_dict = {
            "id": image_counter_value,
            "file_name": file_name,
            "height": height,
            "width": width,
        }
        if is_validation:
            val_images.append(image_dict)
            val_image_counter += 1
        else:
            train_images.append(image_dict)
            train_image_counter += 1

        for annotation in labelbox_image["Label"]["objects"]:
            category_name = annotation["value"]  # or 'title'?
            if category_name not in categories:
                categories[category_name] = category_counter
                category_counter += 1

            annotation_counter_value = (
                val_annotation_counter if is_validation else train_annotation_counter
            )
            a_bbox = annotation["bbox"]
            annotation_dict = {
                "id": annotation_counter_value,
                "image_id": image_counter_value,
                "category_id": 0,
                "bbox": [
                    a_bbox["left"],
                    a_bbox["top"],
                    a_bbox["width"],
                    a_bbox["height"],
                ],
                "iscrowd": 0,
                "area": a_bbox["height"] * a_bbox["width"],
            }

            if is_validation:
                val_annotations.append(annotation_dict)
                val_annotation_counter += 1
            else:
                train_annotations.append(annotation_dict)
                train_annotation_counter += 1

    categories = [
        {"id": category_id, "name": category_name}
        for category_name, category_id in categories.items()
    ]

    _write_coco(
        train_output_file_path,
        train_images,
        train_annotations,
        categories,
        train_image_counter,
        train_annotation_counter,
    )
    _write_coco(
        val_output_file_path,
        val_images,
        val_annotations,
        categories,
        val_image_counter,
        val_annotation_counter,
    )

    test_images = []
    test_image_counter = 0
    for image in all_data["images"]:
        if image["file_name"] not in used_file_names:
            image["id"] = test_image_counter
            test_image_counter += 1
            test_images.append(image)

    all_data["images"] = test_images
    with open(str(test_output_file_path), "w") as file:
        json.dump(all_data, file, indent=config.JSON_INDENT)


def _write_coco(
    train_output_file_path,
    train_images,
    train_annotations,
    categories,
    train_image_counter,
    train_annotations_counter,
):
    train_output_data = {
        "images": train_images,
        "annotations": train_annotations,
        "categories": categories,
    }
    logger.info(
        f"Found {train_image_counter} train images"
        + f" and {train_annotations_counter} train annotations."
    )
    logger.info(f"Saving train coco file to {str(train_output_file_path)}.")
    with open(str(train_output_file_path), "w") as file:
        json.dump(train_output_data, file, indent=config.JSON_INDENT)


def get_conversion_dict(
    client: graphqlclient.client.GraphQLClient, dataset_id: str, model_data
):
    data_rows = get_data_rows(client, dataset_id)
    logging.info(f"Number of rows in dataset on labelbox: {len(data_rows)}")
    image_id_to_filename = {
        image["id"]: image["file_name"] for image in model_data["images"]
    }
    external_id_to_data_row_id = {dr["externalId"]: dr["id"] for dr in data_rows}
    image_id_to_data_row_id = {
        image_id: external_id_to_data_row_id[filename]
        for image_id, filename in image_id_to_filename.items()
    }
    return image_id_to_data_row_id


def get_converted_rows(
    client: graphqlclient.client.GraphQLClient,
    project_id: str,
    model_data: Dict,
    image_id_to_data_row_id: Dict[str, str],
) -> List[Dict]:
    schema_id = get_schema_id(client, project_id)

    output_rows = []
    for annotation in model_data["annotations"]:
        # there is only one category
        assert annotation["category_id"] == 0

        data_row_id = image_id_to_data_row_id[annotation["image_id"]]
        x, y, width, height = annotation["bbox"]

        output_rows.append(
            {
                "uuid": str(uuid.uuid4()),
                "schemaId": schema_id,
                "dataRow": {"id": data_row_id,},
                "bbox": {
                    "top": y,
                    "left": x,
                    "width": width,
                    "height": height,
                    # "top": round(y),
                    # "left": round(x),
                    # "width": round(width),
                    # "height": round(height),
                },
            }
        )

    return output_rows


def get_schema_id(client: graphqlclient.client.GraphQLClient, project_id: str) -> str:
    ontology = get_ontology(client, project_id)
    schema_id = ontology["data"]["project"]["ontology"]["normalized"]["tools"]
    if len(schema_id) > 1:
        raise Exception(f"More than 1 schema found: {ontology}")
    schema_id = schema_id[0]["featureSchemaId"]
    return schema_id


def get_ontology(client: graphqlclient.client.GraphQLClient, project_id: str) -> Dict:
    return _get_json(
        client,
        f"""
            query {{
                project (where:
                    {{id: "{project_id}"}}
                ) {{
                ontology {{
                    normalized }}
                }}
            }}
        """,
    )


def get_data_rows(
    client: graphqlclient.client.GraphQLClient,
    dataset_id: str,
    max_size: int = 5000,
    limit: int = 100,
) -> List[Dict]:
    all_data_rows = []
    for skip in range(0, max_size, limit):
        response = get_data_rows_paginated(client, dataset_id, first=limit, skip=skip)
        limited_data_rows = response["data"]["dataset"]["dataRows"]
        if len(limited_data_rows) == 0:
            break

        all_data_rows.extend(limited_data_rows)

    return all_data_rows


def get_data_rows_paginated(
    client: graphqlclient.client.GraphQLClient, dataset_id: str, first: int, skip: int
) -> Dict:
    return _get_json(
        client,
        f"""
            query GetDataRows{{
                dataset(
                    where:{{
                        id: "{dataset_id}"
                    }}
                ){{
                    dataRows(first: {first}, skip:{skip}){{
                        id
                        externalId
                    }}
                }}
            }}
        """,
    )


def new_data_row_feature_collection(data_row_id: str) -> Dict:
    return {
        "type": "FeatureCollection",
        "properties": {"dataRow": {"id": data_row_id,}},
        "features": [],
    }


def new_data_row_feature(
    schema_id: str, coordinates: List[List[List[List[int]]]]
) -> Dict:
    return {
        "type": "Feature",
        "properties": {"schema": {"id": schema_id,}, "uuid": str(uuid.uuid4()),},
        "geometry": {"type": "Polygon", "coordinates": coordinates,},
    }


def create_bulk_import_request(
    client: graphqlclient.client.GraphQLClient, project_id: str, url: str
) -> str:
    import_request_name = f"model_{datetime.datetime.now().isoformat()}"
    _get_json(
        client,
        f"""
            mutation {{
                createBulkImportRequest(data: {{
                        projectId: "{project_id}",
                        name: "{import_request_name}",
                        fileUrl: "{url}"}}) {{
                    id
                }}
            }}
        """,
    )
    return import_request_name


def get_import_request_update(
    client: graphqlclient.client.GraphQLClient,
    project_id: str,
    import_request_name: str,
) -> Dict:
    return _get_json(
        client,
        f"""
            query {{
                bulkImportRequest(where: {{
                        projectId: "{project_id}",
                        name: "{import_request_name}"}}) {{
                    id
                    name
                    inputFileUrl
                    outputFileUrl
                    state
                }}
            }}
        """,
    )


def pretty_print(data: Dict) -> None:
    logging.info(json.dumps(data, indent=config.JSON_INDENT))


def _get_json(client: graphqlclient.client.GraphQLClient, query: str) -> Dict:
    return json.loads(client.execute(query))
