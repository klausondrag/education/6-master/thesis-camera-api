import logging
import os
from enum import Enum
from pathlib import Path

import pyrealsense2 as rs
from dataclasses import dataclass


class ChannelMode(Enum):
    COLOR = "color"
    DEPTH = "depth"
    INFRARED_1 = "infrared_1"
    INFRARED_2 = "infrared_2"


# as defined here
# https://intelrealsense.github.io/librealsense/python_docs/_generated/pyrealsense2.colorizer.html#pyrealsense2.colorizer  # noqa
class ColorPalette(Enum):
    JET = 0
    CLASSIC = 1
    WHITE_TO_BLACK = 2
    BLACK_TO_WHITE = 3
    BIO = 4
    COLD = 5
    WARM = 6
    QUANTIZED = 7
    PATTERN = 8


class Emitter(Enum):
    NO_CHANGE = -1.0
    DISABLED = 0.0
    ENABLED = 1.0


IS_DEBUGGING = os.getenv("FLASK_DEBUG", False)
LOG_LEVEL = logging.DEBUG if IS_DEBUGGING else logging.INFO
LOG_FORMAT = "%(asctime)s - %(levelname)-8s - %(name)-12s - %(message)s"
logging.basicConfig(format=LOG_FORMAT, level=LOG_LEVEL)
logger = logging.getLogger(__name__)

ENVIRONMENT = os.getenv("FLASK_ENV", "production")
IS_DEV_ENV = ENVIRONMENT == "development"
logger.info(f"Environment: {ENVIRONMENT}")

IMAGE_FILE_EXTENSION = "jpg"
BAG_FILE_EXTENSION = "bag"
NUMPY_FILE_EXTENSION = "npy"

COLOR_PALETTE = ColorPalette.WARM
DEFAULT_PRESET_NAME = "Default"

MAX_PRESET_NAME_LENGTH = len("Medium Density")

DATA_DIR = Path()
RAW_DATA_DIR = Path()
THUMBNAILS_DATA_DIR = Path()
EXTRACTED_FOR_VIS_DATA_DIR = Path()
EXTRACTED_DATA_DIR = Path()
FILTERED_DATA_DIR = Path()
FILTERED_CSV_FILE_PATH = Path()
FLAT_DATA_DIR = Path()
FLAT_IMAGES_DATA_DIR = Path()
FLAT_DEPTH_DATA_DIR = Path()
FLAT_CSV_FILE_PATH = Path()
ALL_JSON_FILE_PATH = Path()
EXAMPLE_MODEL_OUTPUT_JSON_FILE_PATH = Path()
LUX_JSON_FILE_PATH = Path()
NORMALIZATION_FILE_PATH = Path()

LABELBOX_SUFFIX = "labelbox"
LABELBOX_EXTENSION = "ndjson"


def set_root_data_dir(path: str) -> None:
    global DATA_DIR, RAW_DATA_DIR, THUMBNAILS_DATA_DIR
    global EXTRACTED_FOR_VIS_DATA_DIR, EXTRACTED_DATA_DIR
    global FILTERED_DATA_DIR, FILTERED_CSV_FILE_PATH
    global FLAT_DATA_DIR, FLAT_IMAGES_DATA_DIR, FLAT_DEPTH_DATA_DIR, FLAT_CSV_FILE_PATH
    global ALL_JSON_FILE_PATH, EXAMPLE_MODEL_OUTPUT_JSON_FILE_PATH, LUX_JSON_FILE_PATH
    global NORMALIZATION_FILE_PATH

    DATA_DIR = Path(path)
    logger.info(f"Setting root directory to {str(DATA_DIR)}")
    RAW_DATA_DIR = DATA_DIR / "0_raw"
    THUMBNAILS_DATA_DIR = DATA_DIR / "0_raw_thumbnails"
    EXTRACTED_FOR_VIS_DATA_DIR = DATA_DIR / "1_extracted_for_vis"
    EXTRACTED_DATA_DIR = DATA_DIR / "2_extracted"
    FILTERED_DATA_DIR = DATA_DIR / "3_filtered"
    FILTERED_CSV_FILE_PATH = FILTERED_DATA_DIR / "info.csv"
    FLAT_DATA_DIR = DATA_DIR / "4_processed"
    FLAT_IMAGES_DATA_DIR = FLAT_DATA_DIR / "images"
    FLAT_DEPTH_DATA_DIR = FLAT_DATA_DIR / "depth"
    FLAT_CSV_FILE_PATH = FLAT_DATA_DIR / "info.csv"
    ALL_JSON_FILE_PATH = FLAT_DATA_DIR / "all.json"
    EXAMPLE_MODEL_OUTPUT_JSON_FILE_PATH = FLAT_DATA_DIR / "example_model_output.json"
    LUX_JSON_FILE_PATH = FLAT_DATA_DIR / "lux.json"
    NORMALIZATION_FILE_PATH = FLAT_DATA_DIR / "normalization.json"

    RAW_DATA_DIR.mkdir(parents=True, exist_ok=True)
    THUMBNAILS_DATA_DIR.mkdir(parents=True, exist_ok=True)
    EXTRACTED_DATA_DIR.mkdir(parents=True, exist_ok=True)
    EXTRACTED_FOR_VIS_DATA_DIR.mkdir(parents=True, exist_ok=True)
    FILTERED_DATA_DIR.mkdir(parents=True, exist_ok=True)
    FLAT_DATA_DIR.mkdir(parents=True, exist_ok=True)
    FLAT_IMAGES_DATA_DIR.mkdir(parents=True, exist_ok=True)
    FLAT_DEPTH_DATA_DIR.mkdir(parents=True, exist_ok=True)


# needed so that the variables are actually of type Path
# set_root_data_dir("data", dry_run=True)

LABELBOX_API_URL = "https://api.labelbox.com/graphql"
JSON_INDENT = 2


@dataclass()
class StreamConfig:
    WIDTH: int
    HEIGHT: int
    DTYPE: rs.format


@dataclass()
class DeviceConfig:
    FRAME_RATE: int
    COLOR_CONFIG: StreamConfig
    DEPTH_CONFIG: StreamConfig
    INFRARED_1_CONFIG: StreamConfig
    INFRARED_2_CONFIG: StreamConfig
    EMITTER_MODE: Emitter
    PRESET_NAME: str

    @property
    def SKIP_FIRST_N_FRAMES(self):
        return 2 * self.FRAME_RATE


ALIGN_TO_STREAM = rs.stream.color
DEVICES_CONFIGS = {
    "D435I": DeviceConfig(
        COLOR_CONFIG=StreamConfig(1920, 1080, rs.format.rgb8),
        DEPTH_CONFIG=StreamConfig(1280, 720, rs.format.z16),
        INFRARED_1_CONFIG=StreamConfig(1280, 720, rs.format.y8),
        INFRARED_2_CONFIG=StreamConfig(1280, 720, rs.format.y8),
        EMITTER_MODE=Emitter.DISABLED,
        PRESET_NAME="Default",
        FRAME_RATE=15,
    ),
    "D435": DeviceConfig(
        COLOR_CONFIG=StreamConfig(1920, 1080, rs.format.rgb8),
        DEPTH_CONFIG=StreamConfig(1280, 720, rs.format.z16),
        INFRARED_1_CONFIG=StreamConfig(1280, 720, rs.format.y8),
        INFRARED_2_CONFIG=StreamConfig(1280, 720, rs.format.y8),
        EMITTER_MODE=Emitter.DISABLED,
        PRESET_NAME="Default",
        FRAME_RATE=15,
    ),
    "D415": DeviceConfig(
        COLOR_CONFIG=StreamConfig(1920, 1080, rs.format.rgb8),
        DEPTH_CONFIG=StreamConfig(1280, 720, rs.format.z16),
        INFRARED_1_CONFIG=StreamConfig(1280, 720, rs.format.y8),
        INFRARED_2_CONFIG=StreamConfig(1280, 720, rs.format.y8),
        EMITTER_MODE=Emitter.DISABLED,
        PRESET_NAME="Default",
        FRAME_RATE=15,
    ),
}

SAVE_IMAGE_CHANNEL_MODES = [
    ChannelMode.COLOR,
    ChannelMode.DEPTH,
    ChannelMode.INFRARED_1,
    ChannelMode.INFRARED_2,
]

SAVE_RAW_CHANNEL_MODES = [
    ChannelMode.COLOR,
    ChannelMode.DEPTH,
    ChannelMode.INFRARED_1,
    ChannelMode.INFRARED_2,
]
