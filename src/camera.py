import asyncio
import datetime
import logging
import shutil
from pathlib import Path
from typing import Any, List, Optional, Tuple

import cv2
import numpy as np
import pyrealsense2 as rs

from src import config


logging.basicConfig(format=config.LOG_FORMAT, level=config.LOG_LEVEL)
logger = logging.getLogger(__name__)

_rs_align = rs.align(config.ALIGN_TO_STREAM)


def align(frame: rs.pyrealsense2.composite_frame) -> rs.pyrealsense2.composite_frame:
    return _rs_align.process(frame)


class Device:
    serial_number: str
    full_name: str
    short_name: str
    device_config: config.DeviceConfig

    rs_config: rs.pyrealsense2.config
    rs_pipeline: rs.pyrealsense2.pipeline
    rs_pipeline_profile: rs.pyrealsense2.pipeline_profile
    rs_depth_sensor: rs.pyrealsense2.pipeline_profile

    @property
    def unique_name(self):
        return f"{self.short_name}-{self.serial_number}"

    def __init__(self, serial_number: str, full_name: str):
        self.serial_number = serial_number
        self.full_name = full_name

        self.short_name = full_name[full_name.rfind(" ") + 1 :]
        if self.short_name not in config.DEVICES_CONFIGS:
            raise Exception(
                f"Please add {self.short_name}" f" to config.DEVICES_CONFIGS"
            )

        self.device_config = config.DEVICES_CONFIGS[self.short_name]

    def __str__(self):
        return self.unique_name

    def __repr__(self):
        return self.unique_name

    def activate(self) -> None:
        self.rs_config = rs.config()
        self.rs_config.disable_all_streams()
        for stream_type, stream_index, stream_config in zip(
            [rs.stream.color, rs.stream.depth, rs.stream.infrared, rs.stream.infrared],
            [0, 0, 1, 2],
            [
                self.device_config.COLOR_CONFIG,
                self.device_config.DEPTH_CONFIG,
                self.device_config.INFRARED_1_CONFIG,
                self.device_config.INFRARED_2_CONFIG,
            ],
        ):
            self.rs_config.enable_stream(
                stream_type,
                stream_index,
                stream_config.WIDTH,
                stream_config.HEIGHT,
                stream_config.DTYPE,
                self.device_config.FRAME_RATE,
            )

        self.rs_pipeline = rs.pipeline()
        self.rs_config.enable_device(self.serial_number)
        self.rs_pipeline_profile = self.rs_pipeline.start(self.rs_config)
        self.rs_depth_sensor = (
            self.rs_pipeline_profile.get_device().first_depth_sensor()
        )
        self.set_preset()

    def set_preset(self) -> None:
        n_presets = int(
            self.rs_depth_sensor.get_option_range(rs.option.visual_preset).max
        )
        logger.info(f"{self.unique_name} - Preset range: {n_presets}")

        preset_index = None
        for index in range(n_presets):
            preset_name = self.rs_depth_sensor.get_option_value_description(
                rs.option.visual_preset, index
            )
            if preset_name.lower() == self.device_config.PRESET_NAME.lower():
                preset_index = index
                break

        if preset_index is None:
            raise Exception(
                f"{self.unique_name} - "
                f"Preset {self.device_config.PRESET_NAME} not found"
            )

        logger.info(
            f"{self.unique_name} - Setting to preset {preset_name}" f" (index {index})"
        )
        self.rs_depth_sensor.set_option(rs.option.visual_preset, index)

        self.override_emitter()
        self.wait_for_stabilization()

    def override_emitter(self) -> None:
        if self.device_config.EMITTER_MODE == config.Emitter.NO_CHANGE:
            return

        starting_emitter_value = self.rs_depth_sensor.get_option(
            rs.option.emitter_enabled
        )
        logger.info(
            f"{self.unique_name} - " f"Initial emitter value: {starting_emitter_value}"
        )
        if starting_emitter_value != self.device_config.EMITTER_MODE.value:
            self.rs_depth_sensor.set_option(
                rs.option.emitter_enabled, self.device_config.EMITTER_MODE.value
            )
            new_emitter_value = self.rs_depth_sensor.get_option(
                rs.option.emitter_enabled
            )
            logger.info(
                f"{self.unique_name} - " f"New emitter value: {new_emitter_value}"
            )

            if new_emitter_value != self.device_config.EMITTER_MODE.value:
                raise Exception(
                    f"{self.unique_name} - "
                    f"Setting of emitter value went wrong."
                    f" New value is still {new_emitter_value}"
                )

    def wait_for_stabilization(self) -> None:
        logger.info(
            f"{self.unique_name} - Skipping"
            f" {self.device_config.SKIP_FIRST_N_FRAMES} frames"
        )
        for _ in range(self.device_config.SKIP_FIRST_N_FRAMES):
            self.rs_pipeline.wait_for_frames()

    async def save_frame(self, image_dir: Path, image_id: str) -> None:
        picture_datetime, n_tries, unaligned_frame = self._get_frame()
        picture_datetime_string = picture_datetime.isoformat().replace(":", ".")

        rs_saver = rs.save_single_frameset()
        rs_saver.process(unaligned_frame)

        original_file_name = (
            f"RealSense Frameset"
            f" {unaligned_frame.frame_number}"
            f".{config.BAG_FILE_EXTENSION}"
        )
        new_file_name = (
            f"{self.unique_name}"
            f"_{self.device_config.PRESET_NAME}"
            f"_{picture_datetime_string}"
            f".{config.BAG_FILE_EXTENSION}"
        )
        new_file_path = image_dir / new_file_name
        shutil.move(
            original_file_name, str(new_file_path),
        )

        aligned_frame = align(unaligned_frame)

        self._write_frame(
            config.RAW_DATA_DIR,
            image_id,
            self.unique_name,
            self.device_config.PRESET_NAME,
            aligned_frame,
        )

    def _get_frame(
        self,
    ) -> Tuple[datetime.datetime, int, rs.pyrealsense2.composite_frame]:
        n_tries = 1
        while True:
            picture_datetime = datetime.datetime.now()
            frame = self.rs_pipeline.wait_for_frames()
            depth_frame = frame.get_depth_frame()
            color_frame = frame.get_color_frame()
            if depth_frame and color_frame:
                break

            logger.info(f"Retrying getting frame... {n_tries}")
            n_tries += 1

        return picture_datetime, n_tries, frame

    def _write_frame(
        self,
        root_dir: Path,
        image_id: str,
        unique_name: str,
        preset_name: str,
        frame: rs.pyrealsense2.composite_frame,
    ):
        for channel_mode in config.SAVE_IMAGE_CHANNEL_MODES:
            image = self._get_image(frame, channel_mode, apply_colorizer=True)
            file_path = self._get_file_path(
                root_dir,
                image_id,
                unique_name,
                preset_name,
                channel_mode,
                create=True,
                is_image=True,
            )

            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            cv2.imwrite(str(file_path), image)

        for channel_mode in config.SAVE_RAW_CHANNEL_MODES:
            raw = self._get_image(frame, channel_mode, apply_colorizer=False)
            file_path = self._get_file_path(
                root_dir,
                image_id,
                unique_name,
                preset_name,
                channel_mode,
                create=True,
                is_image=False,
            )

            np.save(file_path, raw)

    def _get_image(
        self,
        frame: rs.pyrealsense2.pipeline,
        channel_mode: config.ChannelMode,
        apply_colorizer: bool,
    ) -> np.array:
        if channel_mode == config.ChannelMode.COLOR:
            mode_frame = frame.get_color_frame()
        elif channel_mode == config.ChannelMode.DEPTH:
            mode_frame = frame.get_depth_frame()
            if apply_colorizer:
                mode_frame = rs.colorizer(config.COLOR_PALETTE.value).colorize(
                    mode_frame
                )
        elif channel_mode == config.ChannelMode.INFRARED_1:
            mode_frame = frame.get_infrared_frame(1)
        elif channel_mode == config.ChannelMode.INFRARED_2:
            mode_frame = frame.get_infrared_frame(2)
        else:
            raise Exception()

        return np.asanyarray(mode_frame.get_data())

    def _get_file_path(
        self,
        root_dir: Path,
        image_id: str,
        unique_name: str,
        preset_name: str,
        channel_mode: config.ChannelMode,
        create: bool,
        is_image: bool,
    ) -> Path:
        parent_dir = root_dir / image_id
        if create:
            parent_dir.mkdir(parents=True, exist_ok=True)

        extension = (
            config.IMAGE_FILE_EXTENSION if is_image else config.NUMPY_FILE_EXTENSION
        )
        return parent_dir / (
            f"{unique_name}" f"_{preset_name}" f"_{channel_mode.value}" f".{extension}"
        )


class DeviceManager:
    def __init__(self, enable_all: bool = True):
        self._context = rs.context()
        self.available_devices: List[Device] = []

        self.refresh_connected_devices()
        if len(self.available_devices) == 0:
            logger.warning("No devices connected!")

        if enable_all:
            self.enable_all_devices()

    def refresh_connected_devices(self):
        self.available_devices = [
            Device(
                d.get_info(rs.camera_info.serial_number),
                d.get_info(rs.camera_info.name),
            )
            for d in self._context.devices
            if d.get_info(rs.camera_info.name).lower() != "platform camera"
        ]
        logger.info(f"Connected devices: {self.available_devices}")

    def enable_all_devices(self):
        for d in self.available_devices:
            d.activate()

    def get_frames(self) -> str:
        image_id = datetime.datetime.now().isoformat().replace(":", ".")
        image_dir = config.RAW_DATA_DIR / image_id
        image_dir.mkdir(exist_ok=True, parents=True)

        logger.info(
            f"Start taking pictures asynchronously"
            f" at {datetime.datetime.now().isoformat()}"
        )
        tasks = [d.save_frame(image_dir, image_id) for d in self.available_devices]
        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.gather(*tasks))
        logger.info(
            f"Finished taking pictures asynchronously"
            f" at {datetime.datetime.now().isoformat()}"
        )

        return image_id


def live_pipeline() -> Tuple[rs.pyrealsense2.pipeline, Any]:
    rs_pipeline = rs.pipeline()

    rs_config = rs.config()
    rs_config.enable_stream(rs.stream.color)
    rs_config.enable_stream(rs.stream.depth)

    pipe_profile = rs_pipeline.start(rs_config)
    depth_sensor = pipe_profile.get_device().first_depth_sensor()

    return rs_pipeline, depth_sensor


def file_pipeline(
    file_path: Path, channel_modes: Optional[List[config.ChannelMode]] = None,
) -> rs.pyrealsense2.pipeline:
    if channel_modes is None:
        channel_modes = [config.ChannelMode.COLOR, config.ChannelMode.DEPTH]

    pipeline = rs.pipeline()
    rs_config = rs.config()
    rs_config.enable_device_from_file(str(file_path), False)

    rs_config.disable_all_streams()
    for cm in channel_modes:
        if cm == config.ChannelMode.COLOR:
            rs_config.enable_stream(rs.stream.color)
        elif cm == config.ChannelMode.DEPTH:
            rs_config.enable_stream(rs.stream.depth)
        else:
            raise Exception(f"Unknown ChannelMode {channel_modes[0]}")

    profile = pipeline.start(rs_config)
    device = profile.get_device()
    playback = device.as_playback()
    playback.set_real_time(False)

    return pipeline


def get_aligned_frame_from_bag(
    file_path: Path, max_count=100
) -> rs.pyrealsense2.composite_frame:
    pipeline = file_pipeline(file_path)

    counter = 0
    success = False
    while (not success) and (counter < max_count):
        success, _, unaligned_frame = get_frame_polling(pipeline)
        counter += 1

    if counter >= max_count:
        return None, False

    aligned_frame = align(unaligned_frame)
    return aligned_frame, True


def get_unaligned_frames_from_bag_separately(
    file_path: Path, channel_modes: Optional[List[config.ChannelMode]] = None,
) -> rs.pyrealsense2.composite_frame:
    if channel_modes is None:
        channel_modes = [config.ChannelMode.COLOR, config.ChannelMode.DEPTH]

    channel_frames = []
    for cm in channel_modes:
        success = False
        while not success:
            pipeline = file_pipeline(file_path, channel_modes=[cm])
            success, _, unaligned_frame = get_frame_polling(
                pipeline, channel_modes=[cm]
            )

        channel_frames.append(unaligned_frame)

    return channel_frames, channel_modes


def get_frame(
    rs_pipeline: rs.pyrealsense2.pipeline,
) -> Tuple[int, rs.pyrealsense2.composite_frame]:
    n_tries = 1
    while True:
        frame = rs_pipeline.wait_for_frames()
        depth_frame = frame.get_depth_frame()
        color_frame = frame.get_color_frame()
        if depth_frame and color_frame:
            break

        logger.info(f"Retrying getting frame... {n_tries}")
        n_tries += 1

    return n_tries, frame


def get_frame_polling(
    rs_pipeline: rs.pyrealsense2.pipeline,
    channel_modes: Optional[List[config.ChannelMode]] = None,
    max_n_tries: int = 30_000,
) -> Tuple[bool, int, rs.pyrealsense2.composite_frame]:
    if channel_modes is None:
        channel_modes = [config.ChannelMode.COLOR, config.ChannelMode.DEPTH]
    success = False
    n_tries = 1
    n_channel_modes = len(channel_modes)
    while True:
        if n_tries > max_n_tries:
            logger.debug(f"Max tries reached {n_tries - 1}")
            break

        frame = rs_pipeline.poll_for_frames()
        frame_size = len(frame)
        if frame_size < n_channel_modes:
            n_tries += 1
        else:
            if frame_size > n_channel_modes:
                logger.warning(
                    f"Received more than {n_channel_modes} channels."
                    f" This is unexpected! {n_tries}"
                )

            channels = []
            for cm in channel_modes:
                if cm == config.ChannelMode.COLOR:
                    c = frame.get_color_frame()
                elif cm == config.ChannelMode.DEPTH:
                    c = frame.get_depth_frame()
                else:
                    raise Exception(f"Unknown ChannelMode {cm}")

                channels.append(c)

            if all(c is not None for c in channels):
                success = True
                break
            else:
                logger.warning(f"Not all channels were valid! {n_tries}")

    return success, n_tries, frame


def set_preset(depth_sensor, index: int) -> str:
    preset_name = depth_sensor.get_option_value_description(
        rs.option.visual_preset, index
    )
    logger.info(f"Setting to preset {preset_name} (index {index})")
    depth_sensor.set_option(rs.option.visual_preset, index)

    if config.EMITTER_MODE != config.Emitter.NO_CHANGE:
        _set_emitter(depth_sensor)

    return preset_name


def _set_emitter(depth_sensor):
    starting_emitter_value = depth_sensor.get_option(rs.option.emitter_enabled)
    logger.info(f"Initial emitter value: {starting_emitter_value}")
    if starting_emitter_value != config.EMITTER_MODE.value:
        depth_sensor.set_option(rs.option.emitter_enabled, config.EMITTER_MODE.value)
        new_emitter_value = depth_sensor.get_option(rs.option.emitter_enabled)
        logger.info(f"New emitter value: {new_emitter_value}")

        if new_emitter_value != config.EMITTER_MODE.value:
            raise Exception(
                f"Setting of emitter value went wrong."
                f" New value is still {new_emitter_value}"
            )


def wait_for_stabilization(pipeline: rs.pyrealsense2.pipeline, n_frames: int) -> None:
    logger.info(f"Skipping {n_frames} frames")
    for _ in range(n_frames):
        pipeline.wait_for_frames()


def get_image(
    frame: rs.pyrealsense2.pipeline,
    channel_mode: config.ChannelMode,
    apply_colorizer: bool = True,
) -> np.array:
    if channel_mode == config.ChannelMode.COLOR:
        mode_frame = frame.get_color_frame()
    elif channel_mode == config.ChannelMode.DEPTH:
        mode_frame = frame.get_depth_frame()
        if apply_colorizer:
            mode_frame = rs.colorizer(config.COLOR_PALETTE.value).colorize(mode_frame)
    elif channel_mode == config.ChannelMode.INFRARED_1:
        mode_frame = frame.get_infrared_frame(1)
    elif channel_mode == config.ChannelMode.INFRARED_2:
        mode_frame = frame.get_infrared_frame(2)
    else:
        raise Exception()

    return np.asanyarray(mode_frame.get_data())


def write_frame(
    root_dir: Path,
    image_id: str,
    unique_name: str,
    preset_name: str,
    frame: rs.pyrealsense2.composite_frame,
    save_raw_depth: bool = False,
):
    channel_modes = [
        config.ChannelMode.COLOR,
        config.ChannelMode.DEPTH,
        config.ChannelMode.INFRARED_1,
        config.ChannelMode.INFRARED_2,
    ]
    frames = [frame] * len(channel_modes)
    write_frames(
        root_dir,
        image_id,
        unique_name,
        preset_name,
        frames,
        channel_modes,
        save_raw_depth,
    )


def write_frames(
    root_dir: Path,
    image_id: str,
    unique_name: str,
    preset_name: str,
    frames: List[rs.pyrealsense2.composite_frame],
    channel_modes: List[config.ChannelMode],
    save_raw_depth: bool = False,
):
    for f, cm in zip(frames, channel_modes):
        image = get_image(f, cm)
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        file_path = get_file_path(
            root_dir, image_id, unique_name, preset_name, cm, create=True
        )
        cv2.imwrite(str(file_path), image)

        if save_raw_depth and cm == config.ChannelMode.DEPTH:
            raw_depth = get_image(f, cm, apply_colorizer=False)
            raw_depth_file_path = (
                file_path.parent / f"{file_path.stem}.{config.NUMPY_FILE_EXTENSION}"
            )
            np.save(raw_depth_file_path, raw_depth)


def get_file_path(
    root_dir: Path,
    image_id: str,
    unique_name: str,
    preset_name: str,
    channel_mode: config.ChannelMode,
    create: bool = False,
) -> Path:
    parent_dir = root_dir / image_id
    if create:
        parent_dir.mkdir(parents=True, exist_ok=True)

    return parent_dir / (
        f"{unique_name}"
        f"_{preset_name}"
        f"_{channel_mode.value}"
        f".{config.IMAGE_FILE_EXTENSION}"
    )
