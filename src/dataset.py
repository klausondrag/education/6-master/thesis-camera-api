import json
import logging
import shutil
from pathlib import Path
from typing import List, Optional, Tuple

import click
import cv2
import numpy as np
import pandas as pd
from tqdm import tqdm

from src import camera, config


logging.basicConfig(format=config.LOG_FORMAT, level=config.LOG_LEVEL)
logger = logging.getLogger(__name__)


@click.group()
@click.argument(
    "data-dir",
    default="data",
    type=click.Path(exists=True, file_okay=False, dir_okay=True),
)
def dataset(data_dir: str) -> None:
    config.set_root_data_dir(data_dir)


@dataset.command()
@click.argument(
    "file-path", type=click.Path(exists=True, file_okay=True, dir_okay=False),
)
def check_bag(file_path: str) -> None:
    _, success = camera.get_aligned_frame_from_bag(Path(file_path))
    logger.info("Success" if success else "Failure")


@dataset.command()
@click.option(
    "--separate-channel-extraction/--no-separate-channel-extraction",
    "--sce/--nsce",
    type=bool,
    default=False,
)
@click.option(
    "--camera-name", type=str, default=None,
)
def extract_for_visualization(
    separate_channel_extraction: bool, camera_name: str,
) -> None:
    _extract(separate_channel_extraction, camera_name, extract_to_id=False)


@dataset.command()
@click.option(
    "--separate-channel-extraction/--no-separate-channel-extraction",
    "--sce/--nsce",
    type=bool,
    default=False,
)
@click.option(
    "--camera-name", type=str, default=None,
)
def extract(separate_channel_extraction: bool, camera_name: str,) -> None:
    _extract(separate_channel_extraction, camera_name, extract_to_id=True)


@dataset.command()
def filter_images() -> None:
    if config.FILTERED_CSV_FILE_PATH.exists():
        df_existing = pd.read_csv(config.FILTERED_CSV_FILE_PATH)
        existing_ids = set(df_existing["ID"].values)
    else:
        existing_ids = set()

    images_ids, all_actions = [], []
    id_paths = sorted(config.EXTRACTED_DATA_DIR.iterdir())
    # step through the data reversed because if images were taken
    # in a bad quality, they were retaken. But then we would have already
    # pressed next...
    id_paths = reversed(id_paths)
    id_tqdm = tqdm(id_paths, leave=True, desc="ID", unit="directory")
    for id_path in id_tqdm:
        id_tqdm.set_postfix(id=id_path.stem)
        image_id = id_path.name

        if image_id in existing_ids:
            continue

        (
            color_image_file_path,
            depth_image_file_path,
            raw_depth_file_path,
        ) = get_file_paths(id_path)

        images_ids.append(image_id)
        color_image = cv2.imread(str(color_image_file_path))

        actions, last_pressed_key, color_image, depth_image, raw_depth = get_actions(
            color_image, depth_image_file_path, raw_depth_file_path
        )

        actions = filter_redundant_flips(actions)

        all_actions.append(actions)

        if last_pressed_key == "n":
            save_or_copy(
                actions,
                image_id,
                color_image_file_path,
                depth_image_file_path,
                raw_depth_file_path,
                color_image,
                depth_image,
                raw_depth,
            )

        pretty_actions = get_pretty_action_names(actions, image_id)

        save_or_add_to_csv(image_id, pretty_actions)


@dataset.command()
def flatten() -> None:
    images_ids, unique_names, preset_names = [], [], []
    id_paths = sorted(config.FILTERED_DATA_DIR.iterdir())
    # filter out csv file
    id_paths = [ip for ip in id_paths if ip.is_dir()]
    id_tqdm = tqdm(id_paths, leave=True, desc="ID", unit="directory")
    for id_path in id_tqdm:
        id_tqdm.set_postfix(id=id_path.stem)
        image_id = id_path.name

        new_image = True
        channels_paths = sorted(id_path.iterdir())
        for channel_path in channels_paths:
            unique_name, preset_name, channel_name = channel_path.stem.split("_")
            extension = channel_path.suffix[1:]

            if new_image:
                images_ids.append(id_path.name)
                unique_names.append(unique_name)
                preset_names.append(preset_name)
                new_image = False

            if channel_name == "color" and extension == config.IMAGE_FILE_EXTENSION:
                target_file_path = (
                    config.FLAT_IMAGES_DATA_DIR
                    / f"{image_id}.{config.IMAGE_FILE_EXTENSION}"
                )
            elif channel_name == "depth" and extension == config.NUMPY_FILE_EXTENSION:
                target_file_path = (
                    config.FLAT_DEPTH_DATA_DIR
                    / f"{image_id}.{config.NUMPY_FILE_EXTENSION}"
                )
            else:
                continue

            if not target_file_path.exists():
                shutil.copy(str(channel_path), str(target_file_path))

    df = pd.DataFrame(
        list(zip(images_ids, unique_names, preset_names)),
        columns=["ID", "Camera", "Preset"],
    )

    df.to_csv(config.FLAT_CSV_FILE_PATH, index=False, mode="w", header=True)


@dataset.command()
def data_to_coco() -> None:
    id_paths = sorted(config.FLAT_IMAGES_DATA_DIR.iterdir())
    id_tqdm = tqdm(id_paths, leave=True, desc="ID", unit="directory")

    images = []
    for id_index, id_path in enumerate(id_tqdm):
        image = cv2.imread(str(id_path))
        height, width, _ = image.shape
        images.append(
            {
                "id": id_index,
                "file_name": id_path.name,
                "height": height,
                "width": width,
            }
        )

    output = {
        "images": images,
        "annotations": [],
        "categories": [{"id": 0, "name": "flower"}],
    }

    with open(config.ALL_JSON_FILE_PATH, "w") as file:
        json.dump(output, file, indent=config.JSON_INDENT)


@dataset.command()
def create_example() -> None:
    with open(config.ALL_JSON_FILE_PATH, "r") as file:
        data = json.load(file)

    data["images"] = data["images"][:1]
    data["annotations"] = [
        {
            "id": 0,
            "image_id": 0,
            "category_id": 0,
            "bbox": [0, 0, 10, 10],
            "iscrowd": 0,
            "area": 10 * 10,
        }
    ]

    with open(config.EXAMPLE_MODEL_OUTPUT_JSON_FILE_PATH, "w") as file:
        json.dump(data, file, indent=config.JSON_INDENT)


@dataset.command()
@click.argument(
    "lux-file", type=click.Path(exists=True, file_okay=True, dir_okay=False),
)
@click.option(
    "--skip-first-line/--use-first-line", type=bool, default=True,
)
@click.option(
    "--max-time-delta-in-seconds", type=int, default=2,
)
def read_lux(
    lux_file: str, skip_first_line: bool, max_time_delta_in_seconds: int
) -> None:
    is_first_line = True
    datetimes = []
    lux_values = []
    with open(lux_file, "r") as file:
        for line in file.readlines():
            # example: line "2020-04-20 18:12:16.256259	362	lux"
            if is_first_line:
                is_first_line = False
                if skip_first_line:
                    continue

            datetime_string, lux, _ = line.split("\t")
            datetimes.append(np.datetime64(datetime_string))
            lux_values.append(float(lux))

    datetimes = np.asarray(datetimes)
    lux_values = np.asarray(lux_values)
    print(datetimes.shape, lux_values.shape)

    lower_time_delta = np.timedelta64(-max_time_delta_in_seconds, "s")
    upper_time_delta = np.timedelta64(max_time_delta_in_seconds, "s")

    image_id_to_lux = dict()
    images_paths = sorted(config.FLAT_IMAGES_DATA_DIR.iterdir())
    for image_path in tqdm(images_paths):
        # example: image_path.name "2020-04-20T18:24:05:656556.jpg"
        image_datetime_string = (
            image_path.name[:13]
            + ":"
            + image_path.name[14:16]
            + ":"
            + image_path.name[17:]
        )
        image_datetime = np.datetime64(image_datetime_string)
        time_deltas = datetimes - image_datetime
        mask = lower_time_delta < time_deltas
        mask &= time_deltas < upper_time_delta

        if np.sum(mask) < 1:
            raise Exception(f"Image {image_path} does not have matching lux values")

        image_id_to_lux[image_path.stem] = np.mean(lux_values[mask])

    with open(str(config.LUX_JSON_FILE_PATH), "w") as file:
        json.dump(image_id_to_lux, file, indent=config.JSON_INDENT)

    all_values = list(image_id_to_lux.values())
    logger.info(
        f"Summary of lux values of images:"
        + f" {np.mean(all_values):.2f} mean"
        + f", {np.std(all_values):.2f} standard deviation"
    )


@dataset.command()
def get_normalization(is_float: bool = False) -> None:
    channel_means = np.zeros(3, dtype=np.float64)
    channel_devs = np.zeros(3, dtype=np.float64)
    images_paths = sorted(config.FLAT_IMAGES_DATA_DIR.iterdir())
    n_images = len(images_paths)
    for image_path in tqdm(images_paths):
        image = cv2.imread(str(image_path))
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        image = image.astype(np.float64)

        mean_per_channel = image.mean(axis=0, keepdims=1).mean(axis=1, keepdims=1)
        channel_means += mean_per_channel[0, 0, :]
        devs = (image - mean_per_channel) ** 2
        devs = devs.mean(axis=0, keepdims=1).mean(axis=1, keepdims=1)
        channel_devs += devs[0, 0, :]

    channel_means /= n_images
    channel_devs /= n_images

    channel_devs = np.sqrt(channel_devs)

    if is_float:
        channel_means /= 255
        channel_devs /= 255

    json_dict = {
        "mean": {
            "R": float(channel_means[0]),
            "G": float(channel_means[1]),
            "B": float(channel_means[2]),
        },
        "std": {
            "R": float(channel_devs[0]),
            "G": float(channel_devs[1]),
            "B": float(channel_devs[2]),
        },
    }
    with open(str(config.NORMALIZATION_FILE_PATH), "w") as file:
        json.dump(json_dict, file, indent=config.JSON_INDENT)

    logger.info(
        f"Channel Values Statistics (RGB):"
        + f" {np.array2string(channel_means, separator=', ')} mean"
        + f", {np.array2string(channel_devs, separator=', ')} std"
    )


@dataset.command()
@click.argument(
    "annotations-json", type=click.Path(exists=True, file_okay=True, dir_okay=False),
)
@click.argument(
    "predictions-json", type=click.Path(exists=True, file_okay=True, dir_okay=False),
)
@click.option("--score-threshold", type=float, default=0.5)
def predictions_to_coco(
    annotations_json: str, predictions_json: str, score_threshold: float
) -> None:
    with open(annotations_json, "r") as file:
        annotations_data = json.load(file)

    with open(predictions_json, "r") as file:
        predictions_data = json.load(file)

    # reset annotations
    # in case of a val dataset, this is already filled
    annotations_data["annotations"] = []

    annotations_counter = 0
    for prediction in predictions_data:
        # example prediction:
        """
        {
            "image_id": 0,
            "category_id": 0,
            "bbox": [
                1025.9678955078125,
                728.98779296875,
                107.55029296875,
                102.48284912109375
            ],
            "score": 0.9306827783584595
        }
        """
        if prediction["score"] < score_threshold:
            continue
        _, _, width, height = prediction["bbox"]
        annotations_data["annotations"].append(
            {
                "id": annotations_counter,
                "image_id": prediction["image_id"],
                "category_id": prediction["category_id"],
                "bbox": prediction["bbox"],
                "iscrowd": 0,
                "area": width * height,
            },
        )

        annotations_counter += 1

    predictions_json = Path(predictions_json)
    # output_file = predictions_json.parent / f"{predictions_json.stem}_coco.json"
    output_file = predictions_json.parent / "test_predictions_coco.json"
    with open(str(output_file), "w") as file:
        json.dump(annotations_data, file, indent=config.JSON_INDENT)


def _extract(
    separate_channel_extraction: bool, camera_name: str, extract_to_id: bool,
) -> None:
    print(
        f"Use separate channel extraction (discouraged):"
        f" {separate_channel_extraction}"
    )

    print(camera_name)

    target_dir = (
        config.EXTRACTED_DATA_DIR
        if extract_to_id
        else config.EXTRACTED_FOR_VIS_DATA_DIR
    )

    id_paths = sorted(config.RAW_DATA_DIR.iterdir())
    id_tqdm = tqdm(id_paths, leave=True, desc="ID", unit="directory")
    for id_path in id_tqdm:
        id_tqdm.set_postfix(id=id_path.stem)
        image_id = id_path.name

        presets_paths = sorted(id_path.glob(f"*.{config.BAG_FILE_EXTENSION}"))
        preset_tqdm = tqdm(presets_paths, leave=False, desc="Preset", unit="files")
        for preset_path in preset_tqdm:
            unique_name, preset_name, preset_id = preset_path.stem.split("_")
            if camera_name is not None and camera_name != unique_name:
                continue

            # Add a comma because tqdm calls strip on it
            pretty_preset_name = preset_name.ljust(config.MAX_PRESET_NAME_LENGTH) + ","
            preset_tqdm.set_postfix(preset=pretty_preset_name)

            if _files_exist(target_dir, image_id, unique_name, preset_name):
                continue

            dir_id = preset_id if extract_to_id else image_id
            extract_from_bag(
                target_dir,
                dir_id,
                unique_name,
                preset_name,
                preset_path,
                separate_channel_extraction,
            )


def _files_exist(
    root_dir: Path, image_id: str, unique_name: str, preset_name: str,
) -> bool:
    files_to_be_created = (
        camera.get_file_path(root_dir, image_id, unique_name, preset_name, channel_mode)
        for channel_mode in [config.ChannelMode.COLOR, config.ChannelMode.DEPTH]
    )

    return all((f.exists() for f in files_to_be_created))


def extract_from_bag(
    root_dir,
    image_id,
    unique_name,
    preset_name,
    preset_path,
    separate_channel_extraction,
):
    if separate_channel_extraction:
        (
            channel_frames,
            channel_modes,
        ) = camera.get_unaligned_frames_from_bag_separately(preset_path)

        camera.write_frames(
            root_dir,
            image_id,
            unique_name,
            preset_name,
            channel_frames,
            channel_modes,
            save_raw_depth=True,
        )

    else:
        aligned_frame, success = camera.get_aligned_frame_from_bag(preset_path)

        if not success:
            print(f"Skipping {preset_path}")
            return
            # raise Exception(preset_path)

        camera.write_frame(
            root_dir,
            image_id,
            unique_name,
            preset_name,
            aligned_frame,
            save_raw_depth=True,
        )


def get_file_paths(id_path: Path) -> Tuple[Path, Path, Path]:
    color_image_file_path, depth_image_file_path, raw_depth_file_path = (
        None,
        None,
        None,
    )
    channels_paths = sorted(id_path.iterdir())
    for channel_path in channels_paths:
        unique_name, preset_name, channel_name = channel_path.stem.split("_")
        extension = channel_path.suffix[1:]

        if channel_name == "color" and extension == config.IMAGE_FILE_EXTENSION:
            color_image_file_path = channel_path
        elif channel_name == "depth" and extension == config.NUMPY_FILE_EXTENSION:
            raw_depth_file_path = channel_path
        elif channel_name == "depth" and extension == config.IMAGE_FILE_EXTENSION:
            depth_image_file_path = channel_path
        else:
            raise Exception(channel_path)
    return color_image_file_path, depth_image_file_path, raw_depth_file_path


def get_actions(
    color_image: np.array, depth_image_file: np.array, raw_depth_file: np.array
) -> Tuple[List[str], str, np.array, Optional[np.array], Optional[np.array]]:
    depth_image, raw_depth = None, None
    actions = []
    while True:
        cv2.imshow("Image", color_image)
        pressed_key = cv2.waitKey(0)
        pressed_key &= 0xFF
        pressed_key = chr(pressed_key)

        ESCAPE_CHAR = "\x1b"
        if pressed_key == ESCAPE_CHAR or pressed_key == "q":
            # quit
            exit()
        elif pressed_key == "n":
            # use, next
            break
        elif pressed_key == "b":
            # blurry, skip
            actions.append(pressed_key)
            break
        elif pressed_key == "r":
            # random image, skip
            actions.append(pressed_key)
            break
        elif pressed_key == "a":
            # angle, skip
            actions.append(pressed_key)
            break
        elif pressed_key == "d":
            # duplicate, skip
            actions.append(pressed_key)
            break
        elif pressed_key == "f":
            # flip
            actions.append(pressed_key)
            color_image = np.flip(color_image, 0)
            depth_image = cv2.imread(str(depth_image_file))
            raw_depth = np.load(str(raw_depth_file))
            depth_image = np.flip(depth_image, 0)
            raw_depth = np.flip(raw_depth, 0)

    return actions, pressed_key, color_image, depth_image, raw_depth


def filter_redundant_flips(actions):
    if "f" in actions:
        actions_without_f = [a for a in actions if a != "f"]
        f_actions = [a for a in actions if a == "f"]
        if len(f_actions) % 2 == 0:
            actions = actions_without_f
        else:
            actions = ["f"] + actions_without_f
    return actions


def save_or_copy(
    actions: List[str],
    image_id: str,
    color_image_file_path: Path,
    depth_image_file_path: Path,
    raw_depth_file_path: Path,
    color_image: np.array,
    depth_image: Optional[np.array],
    raw_depth: Optional[np.array],
) -> None:
    target_dir = config.FILTERED_DATA_DIR / image_id
    target_dir.mkdir(parents=True, exist_ok=True)
    target_color_image_file_path = target_dir / color_image_file_path.name
    target_depth_image_file_path = target_dir / depth_image_file_path.name
    target_raw_depth_file_path = target_dir / raw_depth_file_path.name
    if "f" in actions:
        # save transformed data
        cv2.imwrite(str(target_color_image_file_path), color_image)
        cv2.imwrite(str(target_depth_image_file_path), depth_image)
        np.save(target_raw_depth_file_path, raw_depth)
    else:
        # copy file
        for channel_path, target_file_path in zip(
            [color_image_file_path, depth_image_file_path, raw_depth_file_path],
            [
                target_color_image_file_path,
                target_depth_image_file_path,
                target_raw_depth_file_path,
            ],
        ):
            if not target_file_path.exists():
                shutil.copy(str(channel_path), str(target_file_path))


def get_pretty_action_names(actions: List[str], image_id: str) -> List[str]:
    pretty_actions = []
    for a in actions:
        if a == "b":
            pretty_actions.append("blurred")
        elif a == "r":
            pretty_actions.append("random")
        elif a == "a":
            pretty_actions.append("angle")
        elif a == "d":
            pretty_actions.append("duplicate")
        elif a == "f":
            pretty_actions.append("flipped")
        else:
            raise Exception(image_id)
    return pretty_actions


def save_or_add_to_csv(image_id: str, pretty_actions: List[str]) -> None:
    df = pd.DataFrame([(image_id, ",".join(pretty_actions))], columns=["ID", "Action"])
    if config.FILTERED_CSV_FILE_PATH.exists():
        df.to_csv(config.FILTERED_CSV_FILE_PATH, index=False, mode="a", header=False)
    else:
        df.to_csv(config.FILTERED_CSV_FILE_PATH, index=False, mode="w", header=True)
