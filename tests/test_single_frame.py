import os
import shutil
from enum import Enum
from pathlib import Path
from typing import Tuple

import numpy as np
import pyrealsense2 as rs
import pytest

from src import camera, config


class Alignment(Enum):
    ALIGNED = "aligned"
    UNALIGNED = "unaligned"


@pytest.mark.skip(reason="This tests reading bag files which, as we know now, works")
def test_process_file():
    frame_number, live_color_image, live_depth_image = get_live_images()

    unaligned_file_name = (
        f"RealSense Frameset"
        f" {frame_number} unaligned"
        f".{config.BAG_FILE_EXTENSION}"
    )
    file_path = Path(unaligned_file_name)

    file_aligned_frame = camera.get_aligned_frame_from_bag(file_path)
    file_color_image = camera.get_image(file_aligned_frame, config.ChannelMode.COLOR)
    file_depth_image = camera.get_image(file_aligned_frame, config.ChannelMode.DEPTH)

    for a in [Alignment.ALIGNED, Alignment.UNALIGNED]:
        file_path = Path(frame_filename(frame_number, a))
        if file_path.exists():
            os.remove(str(file_path))

    # It appears that there are some minor fluctuations in there.
    # But they seem to be very few so I think it's fine
    assert (live_color_image != file_color_image).sum() < 16
    assert (live_depth_image != file_depth_image).sum() < 16


def frame_filename(frame_number: int, alignment: Alignment) -> str:
    return (
        f"RealSense Frameset"
        f" {frame_number} {alignment.value}"
        f".{config.BAG_FILE_EXTENSION}"
    )


def get_live_images() -> Tuple[int, np.array, np.array]:
    pipeline, _ = camera.live_pipeline()
    _, frame = camera.get_frame(pipeline)

    saver = rs.save_single_frameset()
    saver.process(frame)
    rename_file(frame.frame_number, Alignment.UNALIGNED)

    align_to = rs.stream.color
    align = rs.align(align_to)
    aligned_frame = align.process(frame)

    color_image = camera.get_image(aligned_frame, config.ChannelMode.COLOR)
    depth_image = camera.get_image(aligned_frame, config.ChannelMode.DEPTH)

    return frame.frame_number, color_image, depth_image


def rename_file(frame_number: int, alignment: Alignment) -> None:
    shutil.move(
        f"RealSense Frameset {frame_number}.{config.BAG_FILE_EXTENSION}",
        frame_filename(frame_number, alignment),
    )
